<article <?php post_class();?>>
      <div class="container base-post-search">
            <div class="content-post-search col-6">    
                  <a class="title-post-search" href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>
                  <div class="thumb-img"><?php the_post_thumbnail(''); ?></div>
                  <p> published in <?php echo get_the_date();?> <?php _e('by')?> <?php the_author_posts_link(); ?></p>
                  <p class="search-categories">categories<?php the_category(' '); ?></p>
                  <p><?php the_tags('Tags: ', ', ');?></p>
                  <?php the_excerpt(); ?>
                  <hr>
           </div>
      </div>
</article>


