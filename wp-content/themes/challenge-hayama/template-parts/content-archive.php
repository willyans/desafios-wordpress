<article <?php post_class();?>>            
      <a class="title-post-archive" href="<?php the_permalink();?>"><h2><?php the_title(); ?></h2></a>
      <div class="thumb-archive">
            <?php the_post_thumbnail('large'); ?>
      </div>
      <p><?php _e('published in');?><?php echo get_the_date(); ?> <?php _e('by')?> <?php the_author_posts_link(); ?></p>
      <p><?php the_tags('Tags: ', ', ');?></p>
      <?php the_excerpt(); ?>
      <hr>
</article>