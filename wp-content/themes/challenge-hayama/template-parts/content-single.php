<article <?php post_class();?>>  
    <div class="post-solo container">
        <div class="post-content-solo">
            <div class="thumb-post">
                <?php the_post_thumbnail('large'); ?>
            </div>
                 <h2><?php the_title(); ?></h2>
                 <p><?php the_content(); ?></p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?php _e('published in') ?> <?php echo get_the_date(); ?></li>
                <li class="list-group-item"><?php _e('by') ?> <?php the_author_posts_link(); ?></li>
                <li class="list-group-item"><?php _e('categories:') ?> <?php the_category(' '); ?></li>
                <li class="list-group-item"><?php the_tags('Tags: ', ', ');?></li>
            </ul>
        </div>
    </div>
</article>