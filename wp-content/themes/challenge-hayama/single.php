<?php get_header(); ?>
<div class="single-page container flex">
    <div class="post-solo col-9" >
        <?php
            if(have_posts()):
            while(have_posts()): the_post();?>
               <?php get_template_part( 'template-parts/content','single' ); ?>
            <?php
            endwhile;
                else:
            ?>
            <h2><?php _e('do not have Posts');?></h2>
            <?php
                endif;
            ?>   
        <?php echo get_sidebar('');?> 
    </div>
     
</div>
<?php
if(comments_open() || get_comments_number() ) {
comments_template();
}
?>
<?php get_footer(); ?>