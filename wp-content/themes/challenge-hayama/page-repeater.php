<?php get_header()?>
<div class="container">
        <div class="img-principal-repeater">
            <?php 
                $image = get_field('Img_principal_repeater','option');
                $size = 'large'; // (thumbnail, medium, large, full or custom size)
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size );
                }
            ?>
        </div>

        <div class="block-1-repeat">
            <?php
                // Check rows exists.
                if( have_rows('bloco_repeater_1','option') ):

                    // Loop through rows.
                    while( have_rows('bloco_repeater_1','option') ) : the_row();?>
                       <h2 class="title-block-1"><?php the_sub_field('title_bloco_1');?></h2>
                       <h6 class="content-block-1"><?php the_sub_field('block_1_content');?></h6>
                       <hr>

                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>

        <div class="services-repeat-1 grid">
            <?php
                // Check rows exists.
                if( have_rows('services_repeat','option') ):

                    // Loop through rows.
                    while( have_rows('services_repeat','option') ) : the_row();?>
                       <div class="service-repeat">
                            <div class="img-service-repeater">
                                <?php 
                                    $image = get_sub_field('img_service');
                                    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                                    if( $image ) {
                                        echo wp_get_attachment_image( $image, $size );
                                    }
                                ?>
                                <h2 class="title-block-1"><?php the_sub_field('title_img_service');?></h2>
                                <p class="content-block-1"><?php the_sub_field('content_img_service');?></p>
                                <hr>
                                </div>
                        </div>
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>

        <div class="product-repeat-content <?php the_field("products_positon",'option');?>">
            <?php
                // Check rows exists.
                if( have_rows('product_repeater','option') ):

                    // Loop through rows.
                    while( have_rows('product_repeater','option') ) : the_row();?>
                       <div class="card-deck mb-3 text-center">
                            <div class="card mb-4 shadow-sm">
                            <div class="card-header">
                                <h4 class="my-0 font-weight-normal"><?php the_sub_field('title_product'); ?></h4>
                            </div>
                            <div class="card-body">
                                <h1 class="card-title pricing-card-title"><?php the_sub_field('product_value'); ?><small class="text-muted"><?php the_sub_field('product_time'); ?></small></h1>
                                <ul class="list-unstyled mt-3 mb-4">
                                <li><?php the_sub_field('product_info_1'); ?></li>
                                <li><?php the_sub_field('product_info_2'); ?></li>
                                <li><?php the_sub_field('product_info_3'); ?></li>
                                <li><?php the_sub_field('product_info_4'); ?></li>
                                </ul>
                                <a type="button" href="<?php the_sub_field('button_product_url')?>" class="btn btn-lg btn-block btn-outline-primary"><?php the_sub_field('button_product_text'); ?></a>
                            </div>
                           </div>
                        </div>
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>

        <div class="flip-container"
           ontouchstart="this.classList.toggle('hover');">
            <?php
                // Check rows exists.
                if( have_rows('text_flip_repeat','option') ):

                    // Loop through rows.
                    while( have_rows('text_flip_repeat','option') ) : the_row();?>
                       <div class="flipper">  		
                            <div class="front">  			
                            <!-- Conteúdo da frente --> 
                            <h2><?php the_sub_field('flip_text');?></h2>  		
                            </div>  		
                            <div class="back">  			
                            <!-- Conteúdo do verso -->  
                            <h2 style="background-color:<?php the_sub_field('background_text_flip_color');?>; color:<?php the_sub_field('text_color_flip');?>"><?php the_sub_field('flip_text_back')?></h2>		
                            </div>  	
                        </div> 
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>

        <div class="bloco-color">
         <?php
                // Check rows exists.
                if( have_rows('color_text_end','option') ):

                    // Loop through rows.
                    while( have_rows('color_text_end','option') ) : the_row();?>
                       <div class="color-text-end">
                           <p>
                               <?php the_sub_field('texte_for_ending_color');?>
                           </p>
                       </div>
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
        </div>
    </div>

<?php get_footer()?>