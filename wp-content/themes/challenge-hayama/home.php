<?php get_header(); ?>
<img src=" <?php header_image(); ?>" height="<?php echo get_custom_header()->height;?>" widht="<?php echo get_custom_header()->width;?>">

<section class="container">
    <div class='menu-desafio'>
        <h1><?php echo get_theme_mod('set_menuzinho_title');?></h1>
        <p><?php echo get_theme_mod('set_menuzinho_content');?></p>
        <div class='menu-desafio-buttons flex'>
            <a class="menu-desafio-button1 bg-light"href="<?php echo get_theme_mod('set_menuzinho_button1');?>"><?php echo get_theme_mod('set_menuzinho_buttontext1');?></a>
            <a class="menu-desafio-button2 bg-light" href="<?php echo get_theme_mod('set_menuzinho_button2');?>"><?php echo get_theme_mod('set_menuzinho_buttontext2');?></a>
        </div>
    </div>
</section>

<section class="sidebar-home-init container flex">
    <div class="sidebar-home col-2">
        <?php if (dynamic_sidebar('sidebar_home')) : else : endif; ?>
    </div>
    <div class="sidebar-home-content col-10">
    <?php
                // Check rows exists.
                if( have_rows('content_sidebar_home','option') ):

                    // Loop through rows.
                    while( have_rows('content_sidebar_home','option') ) : the_row();?>
                      <h2><?php the_sub_field('title_content_sidebar_home');?></h2>
                      <h6><?php the_sub_field('text_content_sidebar_home');?></h6>
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>
    </div>
</section>

<section class="services">
    <div class="container">
      <h1> <?php _e('Our services');?></h1>
        <div class="row">
            <div class="item-service col-sm-4">
              <?php
                if( is_active_sidebar('service-1') ){
                    dynamic_sidebar('service-1');
                }
                ?>
            </div>
            <div class="item-service col-sm-4">
              <?php
                if( is_active_sidebar('service-2') ){
                    dynamic_sidebar('service-2');
                }
               ?>
            </div>
            <div class="item-service col-sm-4">
            <?php
                if( is_active_sidebar('service-3') ){
                    dynamic_sidebar('service-3');
                }
                ?>
            </div>
        </div>
    </div>
</section>

<section class="section-slider">
    <div class="slider-post">
         <?php echo do_shortcode('[recent_post_carousel design="design-1"]');?>
    </div>
</section>

<div class="album py-5 bg-light">
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            <?php while(have_posts()) { the_post(); ?>
                <div class="card-group">
                        <div class="card">
                            <div class="card-img-top" alt="Card image cap">
                               <?php the_post_thumbnail(
                                   get_the_Id(), 
                                   ['class' => 'img-responsive responsive--full', 
                                    'title' => 'Feature image']); ?>               
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <div class="card-text post-content"><?php the_excerpt()?></div>
                                <p class="card-text post-catgory"><small class="text-muted">Category:<?php the_category(','); ?></small></p>
                                <p class="card-text post-tag"><small class="text-muted">Tags:<?php the_tags(' '); ?></small></p>
                                <p class="card-text post-author"><small class="text-muted">Author:<?php the_author(); ?></small></p>
                                <div class="btn-group">
                                    <a href='<?php the_permalink(); ?>' class="btn btn-sm btn-outline-secondary">View</a>
                                    <?php edit_post_link('Edit Post',"","", get_the_ID(), 'btn btn-sm btn-outline-secondary');?>
                                </div>
                            </div>
                        </div>
                     </div>
                  <?php
                 }
                ?>
        </div>
        <?php
          the_posts_pagination(
            array( 
                'prev_text' => 'Previous',
                'next_text' =>  'Next'
              )
            );
        ?>
    </div>
</div>
<div class="container about-me">
    <div class="flex">
        <div class="myimg col-6">
            <img src="<?php echo get_theme_mod('pdn_home_img_settings');?>" alt="">
        </div>
        <div class="mytitle">
            <h2>
                <?php echo get_theme_mod('set_about_me_title');?>
            </h2>
        </div>
    </div>
    <div class="mytext col-12">
        <p><?php echo get_theme_mod('set_about_me_text')?></p>
    </div>
</div>

<div>
       <?php
       $key = get_theme_mod('set_map_apikey');
       $address = urlencode(get_theme_mod('set_map_address'));
       ?>
    <iframe
        magin-top="30px"
        width="100%"
        height="450"
        style="border:0"
        loading="lazy"
        allowfullscreen
        referrerpolicy="no-referrer-when-downgrade"
        src="https://www.google.com/maps/embed/v1/place?key=<?php echo $key;?>
        &q=<?php echo $address; ?>&zoom=15">
    </iframe>
</div>
<?php get_footer();?>