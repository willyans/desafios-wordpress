<!DOCTYPE html>
<html lang="pt-br">
<head>
 <?php wp_head(); ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php// wp_title();?>
</head>
<body>
<header>
  <section class="menu-area ">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand col-2" href="<?php echo home_url(); ?>">
        <img src= "<?php echo get_theme_mod('set_icon_menu');?>"
            width="30" height="30" class="d-inline-block align-top" alt="">
            CThuLu
       </a>
       <div class="menu-nav-word col-7">
        <?php
            wp_nav_menu(array(
              'theme_location' => 'my_main_menu',
              'depth' => 1,
              'container' => 'ul',
              'menu_class'=> 'navbar-nav',//ul class 
              'link_class' =>'nav-link',
              'list_item_class'  => 'nav-item active',
            ));
          ?>
       </div>
       <div class="col-3">  
          <?php get_search_form(); ?>
       </div>
   </nav>
  </section>               
</header> 

