<?php get_header(); ?>
<div class="img-init-degrade">
    <?php 
        $image = get_field('img_init','option');
        $size = 'large'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {
            echo wp_get_attachment_image( $image, $size );
        }
    ?>
</div>
<div class="container">
    <div class="inicio-degrade-content" style="background-image: linear-gradient(140deg, <?php the_field('back_ground_color_1','option');?> 0%, <?php the_field('back_ground_color_2','option');?> 50%, <?php the_field('back_ground_color_3','option');?> 75%);">
        <h2><?php the_field('title_degrade','option');?></h2>
        <p><?php the_field('content_prinipal','option');?></p>
    </div>
</div>

<div class="container gallery-degrade">
    <?php 
    $images = get_field('degrade_galery','option');
    $size = 'medium'; // (thumbnail, medium, large, full or custom size)
    if( $images ): ?>
        <ul>
            <?php foreach( $images as $image_id ): ?>
                <li>
                    <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>



<div class="container">
    <div class="inicio-degrade-content" style="background-image: linear-gradient(140deg, <?php the_field('color_picker_degrade','option');?> 0%, <?php the_field('color_picker_degrade_2','option');?> 50%, <?php the_field('color_picker_degrade_3','option');?> 75%);">
        <h2><?php the_field('title_degrade','option');?></h2>
        <p><?php the_field('content_prinipal','option');?></p>
    </div>
</div>

<div class="container grid degrade-img-text" style="background-image: linear-gradient(140deg, <?php the_field('color_picker_degrade_4','option');?> 0%, <?php the_field('color_picker_degrade_5','option');?> 50%, <?php the_field('color_picker_degrade_6','option');?> 75%);">
     <div class="img-degrade-1">
        <?php 
            $image = get_field('degrade_image_1','option');
            $size = 'large'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
                echo wp_get_attachment_image( $image, $size );
            }
        ?>
     </div>
     <div class="text-img-dregrade">
         <p><?php the_field('image_degrade_text','option');?></p>
     </div>
     <div class="img-degrade-2">
        <?php 
                $image = get_field('degrade_image_2','option');
                $size = 'large'; // (thumbnail, medium, large, full or custom size)
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size );
                }
        ?>
     </div>
     <div class="text-img-dregrade">
         <p><?php the_field('image_degrade_text_2','option');?></p>
     </div>
</div>

<div class="container ending-page-degrade" style="background-image: linear-gradient(140deg, <?php the_field('color_picker_degrade_7','option');?> 0%, <?php the_field('color_picker_degrade_8','option');?> 50%, <?php the_field('color_picker_degrade_9','option');?> 75%);">
    <h2><?php the_field('degrade_ending_title','option');?></h2>
    <p><?php the_field('degrade_ending_text','option');?></p>
</div>
<?php get_footer(); ?>