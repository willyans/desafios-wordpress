<footer>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class='col-8'>
                <?php
                wp_nav_menu(array(
                'theme_location' => 'footer_menu',
                        'depth' => 2,
                        'depth' => 1,
                        'container' => 'ul',
                        'menu_class'=> 'navbar-nav',//ul class 
                        'link_class' =>'nav-link',
                        'list_item_class'  => 'nav-item active',
                ));
                ?>
        </div>
        <div class='copyright col-4'>
                <p><?php echo get_theme_mod('set_copyright');?></p>
        </div>
  </nav>
</footer>
</body> 
<?php wp_footer(); ?>
</html>