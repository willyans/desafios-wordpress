<?php
//requrendo o customizer
require get_template_directory() . '/inc/customizer.php';

//carregando scripts e paginas de estilo

function load_scripts(){
    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', array('jquery'),('5.1.0'), true);

    
    wp_enqueue_style( 'bootstrap-css' , 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css', 
    array(), '5.1.3', 'all' );

    wp_enqueue_style( 'template' , get_template_directory_uri() . '/css/template.css', array(), filemtime(get_stylesheet_directory() . '/css/template.css'));
    /*array(), '1.0', 'all' );*/
    
}

add_action('wp_enqueue_scripts', 'load_scripts' );

/* função de configuração de thema*/
function willconf_config() {
    //registrando menus
register_nav_menus(
    array(
        'my_main_menu' => 'Main Menu',
        'footer_menu' => 'Footer Menu'
    )
);
   $args = array(
       'height' => 225,
       'width' => 1920
   );
   add_theme_support('title-tag');
   add_theme_support('custom-header', $args);
   add_theme_support('post-thumbnails');
   add_theme_support('post-formats', array( 'video', 'image', 'status', 'chat'));
   add_theme_support('title-tag');
   add_theme_support('custom-logo', array( 'height' => 110, 'width' => 200));

// Habilitando suporte a tradução
$textdomain = 'willtradu';

load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

//suporte ao gutenberg
add_theme_support('align-wide');
add_theme_support('responsive-embeds');
add_theme_support('editor-color-palette', array(
    array(
        'name' => __('Blood Red', 'wpcurso'),
         'slug' => 'blood-red',
         'color' => '#b9121b'
    ),
    array(
        'name' => __('White', 'wpcurso'),
         'slug' => 'White',
         'color' => '#ffffff'
    ),
    ));
    add_theme_support('editor-styles');
    add_editor_style('./css/style-editor.css');
    //add support woocommerce
    add_theme_support('woocommerce');
    
}

add_action( 'after_setup_theme', 'willconf_config', 0 );

//edit menus



 //thumb tamanho 
add_image_size( 'your-image', 400, 300, array( 'center', 'center' ) );


add_image_size( 'your-image');

add_action( 'widgets_init','wpcurso_sidebars');
function wpcurso_sidebars(){
register_sidebar( array( 'name' => 'Sidebar Hayama',
    'id' => 'sidebar_3',
    'description' => 'sidebar do Hayama',
    'before_width' => '<div class="sidbar-hayama">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="sidebar-hayama-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'Sidebar Home',
'id' => 'sidebar_home',
'description' => 'sidebar for tests',
'before_width' => '<div class="widget-wrapper">',
'after_width' => '</div>',
'before_title' => '<h2 class="width-title">',
'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'Sidebar Features',
'id' => 'sidebar_features',
'description' => 'sidebar for tests contact',
'before_width' => '<div class="widget-wrapper">',
'after_width' => '</div>',
'before_title' => '<h2 class="width-title">',
'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'service1',
    'id' => 'service-1',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'service2',
    'id' => 'service-2',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
register_sidebar( array( 'name' => 'Service3',
    'id' => 'service-3',
    'description' => 'service for tests',
    'before_width' => '<div class="widget-wrapper">',
    'after_width' => '</div>',
    'before_title' => '<h2 class="width-title">',
    'afeter_title' => '</h2>',
)
);
};

//filtro para o search retornar apenas posts_type
function searchFilter($query) {
    if ($query->is_search) {
        if ( !isset($query->query_vars['post_type']) ) {
            $query->set('post_type', 'post');
        }
    }
    return $query;
}
add_filter('pre_get_posts','searchFilter');



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'parent_slug'	=> 'themes.php',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Forms',
		'menu_title'	=> 'Forms',
		'parent_slug'	=> 'themes.php',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Degrade Page',
		'menu_title'	=> 'Degrade Page',
		'parent_slug'	=> 'themes.php',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Menssage Page',
		'menu_title'	=> 'Menssage Page',
		'parent_slug'	=> 'themes.php',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Repeater Page',
		'menu_title'	=> 'Repeater Page',
		'parent_slug'	=> 'themes.php',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Page',
		'menu_title'	=> 'Home Page',
		'parent_slug'	=> 'themes.php',
	));
}


 

