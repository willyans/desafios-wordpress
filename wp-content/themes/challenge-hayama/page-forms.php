<?php get_header(); ?>

<div class="container form-principal-form" style="background-color:<?php the_field('formulario','option');?>; color:<?php the_field('words_color_form','option'); ?>" >
    <h2 class="forms-title"><?php the_field('title_widget','option'); ?></h2>
    <p class='form-content'><?php the_field('widget_content','option'); ?></p>
    <p class="form-content-2"><?php the_field('widget_content_2','option'); ?></p>
</div>

<div class="container form-img">
    <?php 
        $image = get_field('form_img','option');
        $size = 'large'; // (thumbnail, medium, large, full or custom size)
        if( $image ) {
            echo wp_get_attachment_image( $image, $size );
        }
    ?>
</div>
<div class="container form-dados">
    <h2>
        <?php the_field('title_form_dados','option'); ?>
    </h2>
    <div class="names flex">
        <h3 class="name1 col-4"><?php the_field('your_name','option'); ?></h3>
        <h3 class="name2 col-4"><?php the_field('last_name','option'); ?></h3>
    </div>
    <div class="your-email">
        <?php the_field('your_email','option'); ?>
    </div>
    <div class="your-message">
        <p><?php the_field('your_message','option'); ?></p>
    </div>
    <div class="your-buttons grid">
        <a href="<?php the_field('url_button_1','option'); ?>"><?php the_field('text_button_1','option'); ?></a>
        <a href="<?php the_field('url_button_2','option'); ?>"><?php the_field('text_button_2','option'); ?></a>
    </div>
</div>
<?php get_footer(); ?>