<?php get_header(); ?>
<div class='container'>
    <div class="menssage-img-principal">
       <?php 
            $image = get_field('img_principal_menssage','option');
            $size = 'large'; // (thumbnail, medium, large, full or custom size)
            if( $image ) {
                echo wp_get_attachment_image( $image, $size );
            }
        ?>
    </div>
    <hr>
    <div class="container-menssage-with-sidebar grid5">
        <div class='content-menssage-sidebar' style='background-image: url(<?php the_field('init_widget','option');?>);'>
        <?php
                // Check rows exists.
                if( have_rows('content_init_page','option') ):
                    // Loop through rows.
                    while( have_rows('content_init_page','option') ) : the_row();?>
                        <div class="content-widget-repeater">
                            <h2 class="title-content-widget"><?php the_sub_field('text_content_init');?></h2>
                            <p class="text-area-widget"><?php the_sub_field('text_area_init');?></p>
                            <a type="button-widget" href="<?php the_sub_field('button_url_init')?>" class="btn btn-lg btn-block btn-outline-primary"><?php the_sub_field('text_button_init'); ?></a>
                        </div>
                        
                     <?php
                    // End loop.
                    endwhile;

                // No value.
                else :
                    // Do something...
                endif;
            ?>    
        </div>
        <div class="container-sidebar-menssage" style="background-color:<?php the_field('color_from_sidebar','option');?>">
            <?php if (dynamic_sidebar('sidebar_3')) : else : endif; ?>
        </div>
    </div>
   
    <div class="grid menssage-content-secondary" style="background-color: <?php the_field('menssage_content_background_color','option');?>; color: <?php the_field('words_color','option');?>">
        <div class="title-menssage text-content-menssage">
           <h2><?php the_field('menssage_title_principal','option');?></h2>
           <p><?php the_field('menssage_content_text','option');?></p>
           <p><?php the_field('menssage_content_text_2','option');?></p>
        </div>
        <div class="img-menssage-content">
           <?php 
             $image = get_field('img_secondary_menssage','option');
             $size = 'large'; // (thumbnail, medium, large, full or custom size)
             if( $image ) {
                echo wp_get_attachment_image( $image, $size );
            }
            ?>
        </div>
        <div class="gallery-menssage">
            <?php 
            $images = get_field('menssage_galery','option');
            $size = 'medium'; // (thumbnail, medium, large, full or custom size)
            if( $images ): ?>
                <ul>
                    <?php foreach( $images as $image_id ): ?>
                        <li>
                            <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="button-menssage-img">
            <a href="<?php the_field('url_for_img_button','option');?>">
                <?php 
                    $image = get_field('img_button','option');
                    $size = 'large'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    }
                ?>
            </a>
           <h2 style="color: <?php the_field('extra_text_button_color','option');?>"><?php the_field('extra_text_button','option');?></h2>
        </div>
    </div>
    <div class="formulario-menssage grid" style="color: <?php the_field('words_color_form_menssage','option');?>;background-color: <?php the_field('background_color_form','option');?>;">
        <div class="text-form-menssage">
           <h2><?php the_field('title_form_menssage','option');?></h2>
           <h5><?php the_field('text_for_form','option');?></h5>
           <h5><?php the_field('text_for_form_2','option');?></h5>
        </div>
        <div class="formulary-menssage">
           <?php echo do_shortcode('[contact-form-7 id="331" title="Contact form 1"]')?>
        </div>
        <div class="gallery-menssage">
            <?php 
            $images = get_field('menssage_galery_ending','option');
            $size = 'medium'; // (thumbnail, medium, large, full or custom size)
            if( $images ): ?>
                <ul>
                    <?php foreach( $images as $image_id ): ?>
                        <li>
                            <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="menssage-ending-text" style="color:<?php the_field('ending_text_color','option');?>">
            <h1 class="title-menssage-ending"><?php the_field('title_ending','option');?></h1>
            <p class="content-menssage-ending"><?php the_field('ending_text','option');?></p>
        </div>
    </div>
    <hr>
</div>
<?php get_footer(); ?>