<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="archive col-8">
            
            <?php
            the_archive_title('<h1 class="archive-title">','</h1>');
            the_archive_description();

            if(have_posts()):
                while(have_posts()): the_post();

            ?>
            <?php get_template_part('template-parts/content','archive'); ?>
            <?php endwhile; ?>
        <?php
            the_posts_pagination(
         array( 
             'prev_text' => 'Previous',
             'next_text' =>  'Next'
         )
      );
    endif;
      ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>