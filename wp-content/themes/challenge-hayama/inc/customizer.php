<?php

function will_customizer($wp_customize){


    //menu hayama
    $wp_customize->add_section(
        'sec_menuzinho', array(
            'title' => 'Menuzinho',
            'description' => 'Menu do desafio'
        )
    );

   //menu hayama title
    $wp_customize->add_setting(
        'set_menuzinho_title', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_menuzinho_title', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' =>'text',
        )
    );


   //menu hayama text-area
    $wp_customize->add_setting(
        'set_menuzinho_content', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
    $wp_customize->add_control(
        'set_menuzinho_content', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' => 'textarea'
        )
    );
    
    //menu hayama button1
    $wp_customize->add_setting(
        'set_menuzinho_button1', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
    $wp_customize->add_control(
        'set_menuzinho_button1', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' => 'url'
        )
    );

      //menu hayama button2
      $wp_customize->add_setting(
        'set_menuzinho_button2', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
    $wp_customize->add_control(
        'set_menuzinho_button2', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' => 'url'
        )
    );
     //menu hayama buttontext1
     $wp_customize->add_setting(
        'set_menuzinho_buttontext1', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
    $wp_customize->add_control(
        'set_menuzinho_buttontext1', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' => 'text'
        )
    );

      //menu hayama buttontext2
      $wp_customize->add_setting(
        'set_menuzinho_buttontext2', array(
            'type' => 'theme_mod',
            'default' => 'add your text here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
    $wp_customize->add_control(
        'set_menuzinho_buttontext2', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_menuzinho',
            'type' => 'text'
        )
    );

   // copyright 
   $wp_customize->add_section(
        'sec_copyright', array(
            'title' => 'Copyright',
            'description' => 'Copyright Section'
        )
    );

    $wp_customize->add_setting(
        'set_copyright', array(
            'type' => 'theme_mod',
            'default' => 'Copyright X - All rights reserved',
            'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );

    $wp_customize->add_control(
        'set_copyright', array(
            'label' => 'Copyright',
            'description' => 'chose wether to show the services section or not',
            'section' => 'sec_copyright',
            'type' => 'text'
        )
    );

    //map

   $wp_customize->add_section(
      'sec_map', array(
          'title' => 'Map',
          'description' => 'Map Section'
        )
    );
  
  //API key

   $wp_customize->add_setting(
       'set_map_apikey', array(
          'type' => 'theme_mod',
          'default' => '',
          'sanitize_callback' => 'wp_filter_nohtml_kses'
        )
    );

    $wp_customize->add_control(
        'set_map_apikey', array(
           'label' => 'API Key',
           'description' => 'Get your key  <a target="_blank" href="http://console.developers.google.com
           /flows/enableapi?apiid=maps_backend">here</a>',
           'section' => 'sec_map',
           'type' => 'text'
        )
    );
   
    // address

    $wp_customize->add_setting(
        'set_map_address', array(
            'type' => 'theme_mod',
            'default' => '',
            'sanitize_callback' => 'esc_textarea'
        )
    );
    
    $wp_customize->add_control(
        'set_map_address', array(
            'label' => 'Type your address here',
            'description' => 'No special caracters allowed',
            'section' => 'sec_map',
            'type' => 'textarea'
        )
    );


    //icon menu
     $wp_customize->add_section(
        'sec_icon_menu', array(
            'title' => 'Icon Menu',
            'description' => 'To change the menu image, set a img url'
        )
    );

   //icon menu  settings and control
    $wp_customize->add_setting(
        'set_icon_menu', array(
            'type' => 'theme_mod',
            'default' => 'select your img url',
            'sanitize_callback' => 'esc_url_raw'
        )
    );

    $wp_customize->add_control(
        'set_icon_menu', array(
            'label' => 'Type your text here',
            'description' => 'just img',
            'section' => 'sec_icon_menu',
            'type' =>'url',
        )
    );
    
        // about me
        $wp_customize->add_section('pdn_home_section', array(
            'title' => 'About Me',
            'description'   => 'Update home image'
        ));
        
        $wp_customize->add_setting('pdn_home_img_settings', array(
        //default value
        ));
        
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pdn_home_img_control', array(
            'label' => 'Edit My Image',
            'settings'  => 'pdn_home_img_settings',
            'section'   => 'pdn_home_section'
        ) ));

        // text-area about me
        $wp_customize->add_setting(
          'set_about_me_text', array(
            'type' => 'theme_mod',
            'default' => 'add your information here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    
        $wp_customize->add_control(
          'set_about_me_text', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'pdn_home_section',
            'type' => 'textarea'
        )
    );
    //about-me Title
    $wp_customize->add_setting(
        'set_about_me_title', array(
            'type' => 'theme_mod',
            'default' => 'add your title here!',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control(
        'set_about_me_title', array(
            'label' => 'Type your text here',
            'description' => 'No special caracters allowed',
            'section' => 'pdn_home_section',
            'type' =>'text',
        )
    );
    
}
add_action( 'customize_register', 'will_customizer' );
