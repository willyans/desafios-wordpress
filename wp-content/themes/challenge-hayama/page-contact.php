<?php get_header(); ?>
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal"><?php the_field('name_home','option'); ?></h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="<?php the_field('features_link','option'); ?>"><?php the_field('features','option'); ?></a>
        <a class="p-2 text-dark" href="<?php the_field('premium_link','option');?>"><?php the_field('premium','option'); ?></a>
        <a class="p-2 text-dark" href="<?php the_field('support_link','option');?>"><?php the_field('support','option'); ?></a>
        <a class="p-2 text-dark" href="<?php the_field('prices_link','option');?>"><?php the_field('prices','option'); ?></a>
      </nav>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4"><?php the_field('title_content','option'); ?></h1>
      <p class="lead"><?php the_field('content_text','option'); ?></p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center <?php the_field('card_position','option');?>">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?php the_field('title_1','option'); ?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?php the_field('value_1','option'); ?><small class="text-muted"><?php the_field('time_plan','option'); ?></small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li><?php the_field('iten_on_card_1','option'); ?></li>
              <li><?php the_field('iten_2_on_card_1','option'); ?></li>
              <li><?php the_field('iten_3_on_card_1','option'); ?></li>
              <li><?php the_field('iten_4_on_card_1','option'); ?></li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary"><?php the_field('text_button','option'); ?></button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?php the_field('title_2','option'); ?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?php the_field('value_2','option'); ?> <small class="text-muted"><?php the_field('time_plan_2','option'); ?></small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li><?php the_field('iten_on_card_2','option'); ?></li>
              <li><?php the_field('iten_2_on_card_2','option'); ?></li>
              <li><?php the_field('iten_3_on_card_2','option'); ?></li>
              <li><?php the_field('iten_4_on_card_2','option'); ?></li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary"><?php the_field('text_button_2','option'); ?></button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?php the_field('title_3','option'); ?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?php the_field('value_3','option'); ?> <small class="text-muted"><?php the_field('time_plan_3','option'); ?></small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li><?php the_field('iten_on_card_3','option'); ?></li>
              <li><?php the_field('iten_2_on_card_3','option'); ?></li>
              <li><?php the_field('iten_3_on_card_3','option'); ?></li>
              <li><?php the_field('iten_4_on_card_3','option'); ?></li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary"><?php the_field('text_button_3','option'); ?></button>
          </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>